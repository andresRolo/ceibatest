package dominio;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dominio.excepcion.GarantiaExtendidaException;
import dominio.repositorio.RepositorioGarantiaExtendida;
import dominio.repositorio.RepositorioProducto;

public class Vendedor {

    public static final String EL_PRODUCTO_TIENE_GARANTIA = "El producto ya cuenta con una garantia extendida";
    public static final String COD_PRODUCTO_3_VOCALES = "Este producto no cuenta con garant�a extendida";
    public static final String BUILD_GARANTIA_EXT_OBJECT = "Error al contruir el objeto Garantia";
    
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private RepositorioProducto repositorioProducto;
    private RepositorioGarantiaExtendida repositorioGarantia;

    public Vendedor(RepositorioProducto repositorioProducto, RepositorioGarantiaExtendida repositorioGarantia) {
        this.repositorioProducto = repositorioProducto;
        this.repositorioGarantia = repositorioGarantia;

    }

    public void generarGarantia(String codigo, String nombreCliente) throws GarantiaExtendidaException {
    	try {
    		Producto producto = repositorioProducto.obtenerPorCodigo(codigo);
        	if(!this.tieneGarantia(codigo)) {
        		if(this.contarVocalesCodigoProducto(codigo)) {
        			Map<String, Object> map = this.calcularGarantia(producto.getPrecio());
        			GarantiaExtendida garantia = this.buildGarantiaExtendida(map, producto, nombreCliente);
    				repositorioGarantia.agregar(garantia);
            	} else {
            		throw new GarantiaExtendidaException(COD_PRODUCTO_3_VOCALES);
            	}
        	} else {
        		throw new GarantiaExtendidaException(EL_PRODUCTO_TIENE_GARANTIA);
        	}
    	} catch (GarantiaExtendidaException e) {
    		throw new GarantiaExtendidaException(EL_PRODUCTO_TIENE_GARANTIA);
		}
    }

    public GarantiaExtendida buildGarantiaExtendida(Map<String, Object> map, Producto producto, String nombreCliente) throws GarantiaExtendidaException {
		try {
			return new GarantiaExtendida(producto, new Date(), dateFormat.parse(map.get("dateToWarranty").toString()),
					Double.parseDouble(map.get("warrantyPrice").toString()), nombreCliente);
		} catch (NumberFormatException | ParseException e) {
			System.out.println("error ParseException " + e.getMessage());
			throw new GarantiaExtendidaException(BUILD_GARANTIA_EXT_OBJECT);
		}
	}

    public Map<String, Object> calcularGarantia(double precio) { //dateFormat.parse("16/08/2018")
		Map<String, Object> map = new HashMap<String, Object>();
    	try {
			double maxPrice = 500.000;
			double warrantyPrice = 0.0;
			Date currentDate = new Date();
			System.out.println(dateFormat.format(currentDate));
			Calendar cal = Calendar.getInstance();
			cal.setTime(currentDate);
			if(precio > maxPrice) {
				warrantyPrice = precio * 0.2;
				int totalDays = 0;
				while(totalDays < 200) {
					cal.add(Calendar.DATE, 1);
					if(cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
						totalDays++;
					}
				}
				if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					cal.add(Calendar.DATE, 1);
				}
			} else {
				warrantyPrice = precio * 0.1;
				cal.add(Calendar.DATE, 100);
			}
			Date dateToWarranty = cal.getTime();
			System.out.println("warrantyPrice -> " + warrantyPrice);
			System.out.println("dateToWarranty -> " + dateFormat.format(dateToWarranty));
			map.put("warrantyPrice", warrantyPrice);
			map.put("dateToWarranty", dateFormat.format(dateToWarranty));
		} catch(Exception e) {
			System.out.println("exc -> " + e.getMessage());
		}
		return map;
	}

	public boolean contarVocalesCodigoProducto(String codigo) {
    	int count = 0;
        for (int i = 0; i < codigo.length(); i++){
            if (codigo.charAt(i) == 'a' || codigo.charAt(i) == 'e' ||codigo.charAt(i) == 'i' || codigo.charAt(i) == 'o' || codigo.charAt(i) == 'u'
            		|| codigo.charAt(i) == 'A' || codigo.charAt(i) == 'E' ||codigo.charAt(i) == 'I' || codigo.charAt(i) == 'O' || codigo.charAt(i) == 'U'){
                count++;
            }
        }
        System.out.println("total vocales -> " + count);
        if(count == 3) {
        	return false;
        } else {
        	return true;
        }
	}

	public boolean tieneGarantia(String codigo) {
    	Producto producto = repositorioGarantia.obtenerProductoConGarantiaPorCodigo(codigo);
    	if(producto != null) {
    		return true;
    	} else {
    		return false;
    	}
    }

}
